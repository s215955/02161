package application.controller;

import application.model.TimeApp;
import application.model.Worker;
import application.model.Worklog;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;

import java.awt.event.ActionEvent;
import java.beans.EventHandler;
import java.util.List;

/*
    Mads Anton Frost Kolborg Klynderud (s215955)
*/
public class WorklogTabController {
    @FXML
    private ChoiceBox<String> worklogProjectBox;

    @FXML
    private Label worklogProjectLabel;

    @FXML
    private ChoiceBox<String> worklogActivityBox;

    @FXML
    private Label worklogActivityLabel;

    @FXML
    private AnchorPane worklogAnchorPane;

    @FXML
    private AnchorPane worklogInfoPane;

    @FXML
    private AnchorPane worklogChange;

    @FXML
    private Label worklogLabel;

    @FXML
    private TextField worklogHours;

    @FXML
    private TextField worklogName;

    @FXML
    private TextArea worklogDesc;

    @FXML
    private Button worklogConfirmBtn;
    @FXML
    private Button worklogCancelBtn;
    @FXML
    private Button removeLogBtn;

    @FXML
    private Button addLogBtn;

    @FXML
    private Button showEditWorklog;

    @FXML
    private Button incrementWorklog;

    @FXML
    private Button decrementWorklog;
    @FXML
    private ListView<String> worklogList;

    @FXML
    private Label worklogPreviewName;
    @FXML
    private Label worklogPreviewDesc;

    @FXML
    private Label worklogPreviewUser;

    @FXML
    private Label worklogPreviewEstimated;

    @FXML
    private Label worklogPreviewTotal;


    private Boolean isEditMode;

    private Boolean isAddMode;
    private TimeApp app;

    public WorklogTabController(TimeApp app) {
        this.app = app;
    }

    public void initialize() {
        worklogActivityBox.getItems().clear();
        worklogProjectBox.getItems().clear();
        worklogList.getItems().clear();
        worklogChange.setVisible(false);
        worklogInfoPane.setVisible(false);
        initializeProject();
        initializeActivity();
        initializeWorklogs();
        toggleControls();
    }
    public void initializeActivity() {
        if (app.hasSelectedProject() && !app.getAllOngoingActivities().isEmpty()) {
            List<String> activitiesByName = app.getAllOngoingActivities();
            worklogActivityBox.setVisible(true);
            worklogActivityBox.setDisable(false);
            worklogActivityLabel.setVisible(true);
            worklogActivityBox.getItems().clear();
            worklogActivityBox.getItems().setAll(activitiesByName);
            if (app.hasSelectedActivity()) {
                String selectedActivityName = (String) app.getActivityName();
                worklogActivityBox.setValue(selectedActivityName);
            }
        } else {
            worklogActivityBox.setVisible(false);
            worklogActivityBox.setDisable(true);
            worklogActivityBox.getItems().clear();
            worklogActivityLabel.setVisible(false);
        }

        worklogActivityBox.getSelectionModel()
                .selectedItemProperty()
                .addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> changeActivity(newValue));
    }

    public void initializeProject() {
        List<String> projectsByName = app.getAllProjectsByName();
        if (!projectsByName.isEmpty()) {
            worklogProjectBox.setDisable(false);
            worklogProjectLabel.setDisable(false);
            worklogProjectBox.getItems().clear();
            worklogProjectBox.getItems().setAll(projectsByName);
            if (app.hasSelectedProject()) {
                String selectedProjectName = (String) app.getSelectedProjectStatistics().get("name");
                worklogProjectBox.setValue(selectedProjectName);
            }
        } else {
            worklogProjectBox.setDisable(true);
            worklogProjectLabel.setDisable(true);
            worklogProjectBox.getItems().clear();;
        }

        worklogProjectBox.getSelectionModel()
                .selectedItemProperty()
                .addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> changeProject(newValue));
    }

    public void initializeWorklogs() {
        if (app.hasSelectedActivity()) {
            List<String> worklogs = app.getAllWorklogsByName();
            worklogList.getItems().clear();

            if (worklogs.size() == 0) {
                worklogList.setDisable(true);

            } else {
                worklogList.setDisable(false);
                worklogList.getItems().addAll(worklogs);
            }
        }
        toggleControls();
    }

    public void changeProject(String projectName) {
        app.selectProject(projectName);
        initializeActivity();
    }

    public void changeActivity(String activityName) {
        app.selectActivity(activityName);
        initializeWorklogs();
    }

    public void handleViewWorklog() {
        String selectedWorklog = worklogList.getSelectionModel().getSelectedItem();

        if (selectedWorklog != null && app.hasSelectedActivity() && app.hasSelectedProject()) {
            worklogInfoPane.setVisible(true);
            app.selectWorklog(selectedWorklog);
            Worklog currentWorklog = app.getSelectedWorklog();
            worklogPreviewName.setText(currentWorklog.getName());
            worklogPreviewDesc.setText(currentWorklog.getDescription());
            worklogPreviewUser.setText(currentWorklog.getWorker().getName() + " (" + currentWorklog.getWorker().getUsername() + ")");
            worklogPreviewEstimated.setText("Estimated hours: " + Integer.toString(currentWorklog.getTotalEstimated()));
            worklogPreviewTotal.setText("Hours used: " + Integer.toString(currentWorklog.getTotalWorked()));
            toggleControls();
        }
    }

    public void handleShowAddWorklog() {
        isEditMode = false;
        isAddMode = true;
        worklogInfoPane.setVisible(false);
        worklogLabel.setText("Add a new worklog..");
        worklogConfirmBtn.setText("Create");
        worklogHours.setText(null);
        worklogName.setText(null);
        worklogDesc.setText(null);
        worklogChange.setVisible(true);
        worklogConfirmBtn.setOnAction(actionEvent ->  {
            addWorklog();
            handleHideAddAndEdit();
        });
    }

    public void handleShowEditWorklog() {
        Worklog selectedWorklog = app.getSelectedWorklog();
        isEditMode = true;
        isAddMode = false;
        worklogInfoPane.setVisible(false);
        worklogLabel.setText("Edit: " + app.getWorklogName());
        worklogConfirmBtn.setText("Update log");
        worklogHours.setText(Integer.toString(selectedWorklog.getTotalEstimated()));
        worklogName.setText(selectedWorklog.getName());
        worklogDesc.setText(selectedWorklog.getDescription());
        worklogChange.setVisible(true);
        worklogConfirmBtn.setOnAction(actionEvent ->  {
            editSelectedWorklog();
            handleHideAddAndEdit();
        });
    }

    public void handleRemoveSelectedWorklog() {
        app.removeWorklogFromActivity();
    }

    public void handleHideAddAndEdit() {
        isEditMode = false;
        isAddMode = false;
        worklogChange.setVisible(false);
        worklogInfoPane.setVisible(false);
    }

    public void editSelectedWorklog() {
        String hours = worklogHours.getText();
        String name = worklogName.getText();
        String description = worklogDesc.getText();

        if (!hours.isEmpty() && !name.isEmpty() && !description.isEmpty()) {
            app.editSelectedWorklog(name,description,Integer.parseInt(hours));
        }
    }

    public void addWorklog() {
        String hours = worklogHours.getText();
        String name = worklogName.getText();
        String description = worklogDesc.getText();

        if (!hours.isEmpty() && !name.isEmpty() && !description.isEmpty()) {
            app.addWorklogToActivity(name,description,Integer.parseInt(hours));
        }

        if (app.hasSelectedProject() && app.hasSelectedActivity()) {
            initializeWorklogs();
        }
    }

    public void toggleControls() {
        if (app.hasSelectedProject() && app.hasSelectedActivity() && app.hasSelectedWorklog()) {
            removeLogBtn.setDisable(false);
            showEditWorklog.setDisable(false);
            incrementWorklog.setDisable(false);
            decrementWorklog.setDisable(false);
        } else {
            removeLogBtn.setDisable(true);
            showEditWorklog.setDisable(true);
            incrementWorklog.setDisable(true);
            decrementWorklog.setDisable(true);
        }

        if (!app.hasSelectedActivity()) {
            addLogBtn.setDisable(true);
        }

        if (app.hasSelectedActivity() && app.hasSelectedProject()) {
            addLogBtn.setDisable(false);
        }
    }

    public void incrementWorklogEffort() {
        if (app.hasSelectedProject() && app.hasSelectedActivity() && app.hasSelectedWorklog()) {
            app.incrementSelectedWorklogEffort();
            handleViewWorklog();
        }
    }

    public void decrementWorklogEffort() {
        if (app.hasSelectedProject() && app.hasSelectedActivity() && app.hasSelectedWorklog()) {
            app.decrementSelectedWorklogEffort();
            handleViewWorklog();
        }
    }

}
