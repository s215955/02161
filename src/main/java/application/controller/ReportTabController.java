package application.controller;

import application.model.TimeApp;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.AnchorPane;

import java.util.List;

/*
    Mads Anton Frost Kolborg Klynderud (s215955)
*/
public class ReportTabController {
    private TimeApp app;

    @FXML
    private AnchorPane generateAnchorPane;

    @FXML
    private Button generateNewReportBtn;

    @FXML
    private ListView<String> reportProjectList;


    public ReportTabController(TimeApp app) {
        this.app = app;
    }

    public void initialize() {
        useEffect();
    }

    public void useEffect() {
        populateReportProjectsList();
        toggleGenerateReportView();
    }

    public void populateReportProjectsList() {
            List<String> projects = app.getAllProjectsByName();
            reportProjectList.getItems().clear();

            if (projects.size() == 0) {
                reportProjectList.setDisable(true);

            } else {
                reportProjectList.setDisable(false);
                reportProjectList.getItems().addAll(projects);
            }

        toggleControls();
    }

    public void toggleGenerateReportView() {
        if (app.getAllProjectsByName().isEmpty()) {
            generateNewReportBtn.setDisable(true);
        } else {
            generateNewReportBtn.setDisable(false);
        }
    }

    public void handleShowGenerateReport() {
        generateAnchorPane.setVisible(true);
    }

    public void handleCloseGenerateReport() {
        generateAnchorPane.setVisible(false);
    }

    public void toggleControls() {

    }
}

