package application.controller;
import application.model.TimeApp;
import application.model.Worker;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/*
    Mads Anton Frost Kolborg Klynderud (s215955)
*/
public class ProjectTabController {

    @FXML
    private ListView<String> projectList;

    @FXML
    private Button removeProjectBtn;

    @FXML
    private Button renameProjectBtn;

    @FXML
    private Button addProjectLeaderBtn;

    @FXML
    private ListView projectLeaderSelect;

    @FXML
    private Label projectLeadersLabel;

    @FXML
    private Label selectedProjectName;

    @FXML
    private AnchorPane selectedProject;

    @FXML
    private Label selectedProjectDesc;

    @FXML
    private ProgressBar selectedProjectProgress;

    private TimeApp app;

    public ProjectTabController(TimeApp app) {
        this.app = app;
    }

    public void initialize() {
        useEffect();
    }
    @FXML
    private void handleAddProject() {
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("Add new project");
        dialog.setHeaderText("Please enter the project name:");
        dialog.setContentText("Project Name:");

        Optional<String> result = dialog.showAndWait();
        result.ifPresent(projectName -> {
            if (!projectName.isEmpty()) {
                app.addProject(projectName);
            }
        });
        useEffect();
    }

    @FXML
    private void handleRenameProject() {
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("Rename selected project: " + app.getProjectName());
        dialog.setHeaderText("Please enter the new project name:");
        dialog.setContentText("Project Name:");

        Optional<String> result = dialog.showAndWait();
        result.ifPresent(projectName -> {
            if (!projectName.isEmpty()) {
                app.setProjectName(projectName);
            }
        });
        useEffect();
    }

    @FXML
    private void handleRemoveProject() {
        app.removeProject();
        useEffect();
    }

    @FXML
    private void handleSelectedProject() {
        String selectedProjectName = projectList.getSelectionModel().getSelectedItem();

        if (selectedProjectName == null || selectedProjectName.isEmpty()) {
            removeProjectBtn.setDisable(true);
            renameProjectBtn.setDisable(true);
            selectedProject.setVisible(false);
        }

        if (selectedProjectName != null) {
            app.selectProject(selectedProjectName);
            removeProjectBtn.setDisable(false);
            selectedProject.setVisible(true);
            renameProjectBtn.setDisable(false);
        }
        renderSelectedProject();
    }

    @FXML
    private void renderSelectedProject() {
        if (app.hasSelectedProject()) {
            Map<String, Object> project = app.getSelectedProjectStatistics();
            String projectName = (String) project.get("name");
            int workLeft = (int) project.get("workLeft");
            int total = (int) project.get("totalActivities");
            int completed= (int) project.get("completedActivities");

            selectedProjectName.setText("Project: " + projectName);
            if (total == 0) {
                selectedProjectDesc.setText(String.format("%s has no activities made yet, go to the activities tab to assign an activity.", projectName));
            } else if (completed == total) {
                double perCompleted = (double) completed / total;
                selectedProjectDesc.setText(String.format("All %s activities completed, with an over/under estimation of %s", total, workLeft));
                selectedProjectProgress.setProgress(perCompleted);
            } else {
                double perCompleted = (double) completed / total;
                selectedProjectDesc.setText(
                        String.format("%s has %s/%s completed activities, with %s hours work left. Below you can see a list of each activity. To add, remove or edit an activity press the \"activities\" tab.",
                                projectName, completed, total, workLeft)
                );
                selectedProjectProgress.setProgress(perCompleted);
            }

            renderSelectedProjectLeaders();
        }
    }

    @FXML
    private void renderProjectsList() {
        List<String> projectListItems = app.getAllProjectsByName();
        if (projectListItems.size() == 0) {
            projectList.setDisable(true);
        } else {
            projectList.setDisable(false);
        }

        projectList.getItems().clear();
        for (String projectName : projectListItems) {
            projectList.getItems().add(projectName);
        }
    }

    @FXML
    private void renderSelectedProjectLeaders() {
        List<String> potentialLeaders = app.getAllWorkersByName();
        List<Worker> leaders = app.getSelectedProjectLeaders();
        if (potentialLeaders.size() > 0) {
            projectLeaderSelect.setDisable(false);
        }
        projectLeaderSelect.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        projectLeaderSelect.getItems().clear();
        for (String leader : potentialLeaders) {
            projectLeaderSelect.getItems().add(leader);
            for (Worker w : app.getWorkers()) {
                if (w.getName().equals(leader) && leaders.contains(w)) {
                    projectLeaderSelect.getSelectionModel().select(leader);
                }
            }
        }
    }

    @FXML
    private void handleSelectedProjectLeaders() {
        List<String> selectedLeaders = projectLeaderSelect.getSelectionModel().getSelectedItems();
        app.setLeadersInSelected(selectedLeaders);
    }

    private void useEffect() {
        renderProjectsList();
        handleSelectedProject();
        renderSelectedProject();
    }

}
