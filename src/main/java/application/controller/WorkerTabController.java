package application.controller;

import application.model.TimeApp;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;

import java.util.List;
import java.util.Optional;

/*
    Mads Anton Frost Kolborg Klynderud (s215955)
*/
public class WorkerTabController {
    @FXML
    private ListView<String> workerList;

    @FXML
    private Button addNewWorker;

    @FXML
    private Button editWorkerName;

    @FXML
    private Button editWorkerUsername;

    @FXML
    private Button removeWorker;

    private String newName;

    private String newUsername;
    private TimeApp app;

    public WorkerTabController(TimeApp app) {
        this.app = app;
    }

    public void initialize() {
        useEffect();
    }

    public void handleSelectedWorker() {
        String selectedWorkerUsername = workerList.getSelectionModel().getSelectedItem();
        if (selectedWorkerUsername != null) {
            app.selectWorker(selectedWorkerUsername.split("\\s*-\\s*")[1]);
        }
        toggleActions();

    }

    @FXML
    private void handleRenameName() {
        TextInputDialog newNameDialog = new TextInputDialog();
        newNameDialog.setTitle("Rename: " + app.getWorkerUsername() + " name.");
        newNameDialog.setHeaderText("Rename: " + app.getWorkerUsername() + " name.");
        newNameDialog.setContentText("Worker Name:");
        Optional<String> result = newNameDialog.showAndWait();
        result.ifPresent(name -> {
            if (!name.isEmpty()) {
                app.setWorkerName(name);
            }
        });
        useEffect();
    }

    @FXML
    private void handleRenameUsername() {
        TextInputDialog newNameDialog = new TextInputDialog();
        newNameDialog.setTitle("Rename: " + app.getWorkerUsername() + " username.");
        newNameDialog.setHeaderText("Rename: " + app.getWorkerUsername() + " username.");
        newNameDialog.setContentText("Worker Username:");
        Optional<String> result = newNameDialog.showAndWait();
        result.ifPresent(username -> {
            if (!username.isEmpty()) {
                app.setWorkerUsername(username);
            }
        });
        useEffect();
    }

    @FXML
    private void handleRemoveWorker() {
        app.removeWorker();
        useEffect();
    }

    @FXML
    private void handleAddWorker() {
        TextInputDialog nameDialog = new TextInputDialog();
        TextInputDialog usernameDialog = new TextInputDialog();
        nameDialog.setTitle("New worker");
        nameDialog.setHeaderText("Please enter the workers full name:");
        nameDialog.setContentText("Name:");

        usernameDialog.setTitle("New worker");
        usernameDialog.setHeaderText("Please enter the workers username (e.g: mafr):");
        usernameDialog.setContentText("Username:");

        nameDialog.showAndWait().ifPresent(name -> {
            if (!name.isEmpty()) {
                this.newName = name;
            }
        });

        usernameDialog.showAndWait().ifPresent(username -> {
            if (!username.isEmpty()) {
                this.newUsername = username;
            }
        });

        app.addWorker(this.newUsername, this.newName);
        useEffect();
    }
    public void renderWorkerList() {
        List<String> workerNameAndUser = app.getAllWorkersByNameAndUsername();
        if (!workerNameAndUser.isEmpty()) {
            workerList.getItems().clear();
            workerList.getItems().addAll(workerNameAndUser);
            workerList.setDisable(false);
        } else {
            workerList.getItems().clear();
            workerList.setDisable(true);
        }
    }

    public void toggleActions() {
        if (app.hasSelectedWorker()) {
            editWorkerName.setDisable(false);
            editWorkerUsername.setDisable(false);
            if (app.selectedWorkerNotUser()) {
                removeWorker.setDisable(false);
            }
        } else {
            editWorkerName.setDisable(true);
            editWorkerUsername.setDisable(true);
            removeWorker.setDisable(true);
        }
    }
    public void useEffect() {
        renderWorkerList();
        toggleActions();
    }
}
