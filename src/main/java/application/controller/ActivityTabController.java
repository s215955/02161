package application.controller;

import application.model.TimeApp;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/*
    Mads Anton Frost Kolborg Klynderud (s215955)
 */
public class ActivityTabController {
    @FXML
    private ChoiceBox<String> projectChoiceBox;

    @FXML
    private AnchorPane projectOverviewBox;

    @FXML
    private AnchorPane selectedActivity;
    @FXML
    private AnchorPane projectOverviewStats;

    @FXML
    private Label activityLabel;
    @FXML
    private ProgressIndicator activityProgress;

    @FXML
    private ProgressIndicator projectProgress;

    @FXML
    private ListView progressActivities;
    @FXML
    private ListView doneActivities;

    @FXML
    private Label finishedLabel;

    @FXML
    private Label progressLabel;

    @FXML
    private Button removeActivityBtn;

    @FXML
    private Button renameActivityBtn;

    @FXML
    private Button addActivityBtn;

    @FXML
    private Label selectedActivityLabel;

    @FXML
    private CheckBox selectedActivityCheckBox;

    @FXML
    private DatePicker selectedActivityDatePicker;
    @FXML
    private TimeApp app;

    private String newName;

    private int newDeadline;

    public ActivityTabController(TimeApp app) {
        this.app = app;

    }

    public void initialize() {
        initializeActivity();
        handleRenderSelectedProjectActivity();
        if (app.hasSelectedProject()) {
            projectChoiceBox.getSelectionModel().select(app.getProjectName());
            addActivityBtn.setDisable(false);
        } else {
            addActivityBtn.setDisable(true);
        }
    }
    public void initializeActivity() {
        List<String> projectsByName = app.getAllProjectsByName();
        if (!projectsByName.isEmpty()) {
            projectChoiceBox.setDisable(false);
            projectChoiceBox.getItems().clear();
            projectChoiceBox.getItems().setAll(projectsByName);
            if (app.hasSelectedProject()) {
                String selectedProjectName = (String) app.getSelectedProjectStatistics().get("name");
                projectChoiceBox.setValue(selectedProjectName);
            }
        } else {
            projectChoiceBox.setDisable(true);
            projectChoiceBox.getItems().clear();
        }

        projectChoiceBox.getSelectionModel()
                .selectedItemProperty()
                .addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> changeProject(newValue));
    }

    public void removeActivity() {
        if (app.hasSelectedActivity()) {
            app.removeActivityFromProject();
        }
        useEffect();
    }

    public void handleToggleComplete() {
        if (app.hasSelectedActivity()) {
            app.toggleActivityCompletion();
        }
        useEffect();
    }

    public void handleChangeDate() {
        app.setSelectedActivityDeadline(selectedActivityDatePicker.getValue());
    }

    public void changeProject(String projectName) {
        String selected = projectChoiceBox.getValue();
        app.selectProject(selected);
        if (app.hasSelectedProject()) {
            renderActivityList();
            handleRenderSelectedProjectActivity();
            addActivityBtn.setDisable(false);
        } else {
            addActivityBtn.setDisable(true);
        }
    }

    @FXML
    private void handleAddActivity() {
        TextInputDialog nameDialog = new TextInputDialog();
        TextInputDialog deadlineDialog = new TextInputDialog();
        nameDialog.setTitle("New activity name");
        nameDialog.setHeaderText("Please enter the activity name:");
        nameDialog.setContentText("Activity Name:");

        deadlineDialog.setTitle("New activity deadline");
        deadlineDialog.setHeaderText("Deadline in how many days?:");
        deadlineDialog.setContentText("Deadline:");

        Optional<String> result = nameDialog.showAndWait();
        result.ifPresent(activityName -> {
            if (!activityName.isEmpty()) {
                this.newName = activityName;
            }
        });

        deadlineDialog.showAndWait().ifPresent(deadline -> {
            if (!deadline.isEmpty()) {
                this.newDeadline = Integer.parseInt(deadline);
            }
        });

        app.addActivity(newName, newDeadline);
        renderActivityList();
    }

    @FXML
    private void handleRenameActivity() {
        TextInputDialog newNameDialog = new TextInputDialog();
        newNameDialog.setTitle("Rename: " + app.getActivityName());
        newNameDialog.setHeaderText("Please enter the activity name:");
        newNameDialog.setContentText("Activity Name:");
        Optional<String> result = newNameDialog.showAndWait();
        result.ifPresent(activityName -> {
            if (!activityName.isEmpty()) {
                app.setActivityName(activityName);
            }
        });
        useEffect();
    }

    @FXML
    private void handleSelectedActivityInProgress() {
        String selectedActivityName = (String) progressActivities.getSelectionModel().getSelectedItem();
        if (selectedActivityName == null || selectedActivityName.isEmpty()) {
            removeActivityBtn.setDisable(true);
            renameActivityBtn.setDisable(true);
            selectedActivity.setVisible(false);
        }

        if (selectedActivityName != null) {
            app.selectActivity(selectedActivityName);
            removeActivityBtn.setDisable(false);
            selectedActivity.setVisible(true);
            renameActivityBtn.setDisable(false);
        }
        handleRenderSelectedProjectActivity();
    }

    @FXML
    private void handleSelectedActivityInFinished() {
        String selectedActivityName = (String) doneActivities.getSelectionModel().getSelectedItem();
        if (selectedActivityName == null || selectedActivityName.isEmpty()) {
            removeActivityBtn.setDisable(true);
            renameActivityBtn.setDisable(true);
            selectedActivity.setVisible(false);
        }

        if (selectedActivityName != null) {
            app.selectActivity(selectedActivityName);
            removeActivityBtn.setDisable(false);
            selectedActivity.setVisible(true);
            renameActivityBtn.setDisable(false);
        }
        handleRenderSelectedProjectActivity();
    }
    public void handleRenderSelectedProjectActivity() {
        if (app.hasSelectedProject()) {
            projectOverviewBox.setVisible(true);
            projectOverviewStats.setVisible(true);

            Map<String, Object> selectedProjectStats = app.getSelectedProjectStatistics();
            int completed = (int) selectedProjectStats.get("completedActivities");
            int total = (int) selectedProjectStats.get("totalActivities");
            finishedLabel.setText("Finished: " + selectedProjectStats.get("completedActivities"));
            progressLabel.setText("In progress: " + app.getAllOngoingActivities().size());

            if (app.hasSelectedActivity()) {
                selectedActivityCheckBox.setSelected(app.getActivityCompletion());
                selectedActivityLabel.setText("Activity: " + app.getActivityName());
                selectedActivity.setVisible(true);
                activityProgress.setVisible(true);
                activityLabel.setVisible(true);
                activityProgress.setProgress(app.getSelectedActivityProgress());
                selectedActivityDatePicker.setValue(app.getSelectedActivityDeadline());
            } else {
                selectedActivityCheckBox.setSelected(false);
                activityLabel.setVisible(false);
                selectedActivity.setVisible(false);
                activityProgress.setVisible(false);
            }
            if (completed != 0 && total != 0) {
                double perCompleted = (double) completed/total;
                projectProgress.setProgress(perCompleted);
            } else {
                projectProgress.setProgress(0);
            }


        } else {
            projectOverviewBox.setVisible(false);
            projectOverviewStats.setVisible(false);
        }
    }

    public void renderActivityList() {
        List<String> completedActivites = app.getAllCompletedActivities();
        List<String> ongoingActivities = app.getAllOngoingActivities();
        doneActivities.getItems().clear();
        progressActivities.getItems().clear();

        if (completedActivites.size() == 0) {
            doneActivities.setDisable(true);
        } else {
            doneActivities.setDisable(false);
            doneActivities.getItems().addAll(completedActivites);
        }

        if (ongoingActivities.size() == 0) {
            progressActivities.setDisable(true);

        } else {
            progressActivities.setDisable(false);
            progressActivities.getItems().addAll(ongoingActivities);
        }
    }

    public void useEffect() {
        handleRenderSelectedProjectActivity();
        renderActivityList();
    }
}
