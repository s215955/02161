package application.controller;

import application.model.Activity;
import application.model.TimeApp;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;

import java.io.IOException;

/*
    Mads Anton Frost Kolborg Klynderud (s215955)
*/
public class MainLayoutController {

    @FXML
    private TabPane rootAppPane;

    @FXML
    private BorderPane rootLoginPane;

    @FXML
    private AnchorPane navHeader;

    @FXML
    private Separator seperator;

    @FXML
    private TextField usernameField;

    @FXML
    private Label loginErrorLabel;

    @FXML
    private Label createProjectLabel;

    private TimeApp app;

    public MainLayoutController() {
        this.app = new TimeApp();
    }

    @FXML
    public void initialize() {
        initializeProjectTab();
        initializeActivityTab();
        initializeWorkerTab();
        initializeWorklogTab();
        initializeReportTab();
    }


    public void initializeProjectTab() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/ProjectTab.fxml"));
            ProjectTabController projectTabController = new ProjectTabController(app);
            loader.setController(projectTabController);
            Tab projectTab = new Tab("Projects");
            projectTab.setOnSelectionChanged(event -> {
                if (projectTab.isSelected()) {
                    projectTabController.initialize();
                    warnAboutNoProjects();
                }
            });
            projectTab.setContent(loader.load());
            rootAppPane.getTabs().add(projectTab);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void initializeActivityTab() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/ActivityTab.fxml"));
            ActivityTabController activityTabController = new ActivityTabController(app);
            loader.setController(activityTabController);
            Tab activityTab = new Tab("Activities");
            activityTab.setOnSelectionChanged(event -> {
                if (activityTab.isSelected()) {
                    activityTabController.initialize();
                    warnAboutNoProjects();
                }
            });
            activityTab.setContent(loader.load());
            rootAppPane.getTabs().add(activityTab);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void initializeWorkerTab() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/WorkerTab.fxml"));
            WorkerTabController workerTabController = new WorkerTabController(app);
            loader.setController(workerTabController);
            Tab workerTab = new Tab("Workers");
            workerTab.setOnSelectionChanged(event -> {
                if (workerTab.isSelected()) {
                    workerTabController.initialize();
                    warnAboutNoProjects();
                }
            });
            workerTab.setContent(loader.load());
            rootAppPane.getTabs().add(workerTab);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void initializeWorklogTab() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/WorklogTab.fxml"));
            WorklogTabController worklogTabController = new WorklogTabController(app);
            loader.setController(worklogTabController);
            Tab worklogTab = new Tab("Worklogs");
            worklogTab.setOnSelectionChanged(event -> {
                if (worklogTab.isSelected()) {
                    worklogTabController.initialize();
                    warnAboutNoProjects();
                }
            });
            worklogTab.setContent(loader.load());
            rootAppPane.getTabs().add(worklogTab);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void initializeReportTab() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/ReportTab.fxml"));
            ReportTabController reportTabController = new ReportTabController(app);
            loader.setController(reportTabController);
            Tab reportTab = new Tab("Reports");
            reportTab.setOnSelectionChanged(event -> {
                if (reportTab.isSelected()) {
                    reportTabController.initialize();
                    warnAboutNoProjects();
                }
            });
            reportTab.setContent(loader.load());
            rootAppPane.getTabs().add(reportTab);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void login() {
        boolean didLogin = app.login(usernameField.getText());

        if (didLogin) {
            rootLoginPane.setVisible(false);
            rootAppPane.setVisible(true);
            seperator.setVisible(true);
            navHeader.setVisible(true);
        }
    }

    public void logout() {
        app.logout();
        rootLoginPane.setVisible(true);
        rootAppPane.setVisible(false);
        seperator.setVisible(false);
        navHeader.setVisible(false);
    }

    public void warnAboutNoProjects() {
        if (app.getAllProjectsByName().isEmpty()) {
            createProjectLabel.setVisible(true);
        } else {
            createProjectLabel.setVisible(false);
        }
    }
}
