package application.model;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

/*
    Mads Anton Frost Kolborg Klynderud (s215955)
*/
public class TimeApp {

    Worker user;
    List<Worker> workers = new ArrayList<>();
    List<Project> projects = new ArrayList<>();

    Project selectedProject;

    Worker selectedWorker;

    Worklog selectedWorklog;

    Activity selectedActivity;

    public TimeApp() {
        Worker admin = new Worker("Admin User", "admin");
        workers.add(admin);
    }

    public Boolean login(String username) {
        for (Worker worker : workers) {
            if (worker.getUsername().equals(username)) {
                user = worker;
                return worker.getUsername().equals(username);
            }
        }

        return false;
    }

    public String getUserName() {
        return user.getName();
    }
    public void logout() {
        user = null;
    }

    public List<String> getAllWorkersByName() {
        return workers.stream()
                .map(Worker::getName)
                .collect(Collectors.toList());
    }

    public List<String> getAllWorkersByNameAndUsername() {
        return workers.stream()
                .map(worker -> {
                    if (user != null && worker.getUsername().equals(user.getUsername())) {
                        return worker.getName() + " - " + worker.getUsername() + " (you)";
                    }
                    return worker.getName() + " - " + worker.getUsername();
                })
                .collect(Collectors.toList());
    }

    public List<String> getAllProjectsByName() {
        return projects.stream()
                .map(Project::getName)
                .collect(Collectors.toList());
    }

    public List<String> getAllWorklogsByName() {
        return selectedActivity.getWorklogs().stream()
                .map(Worklog::getName)
                .collect(Collectors.toList());
    }
    public void addProject(String name) {
        boolean doesNotExist = projects.stream()
                .noneMatch(project -> project.getName().equals(name));

        if (doesNotExist) {
            projects.add(new Project(name));
        }
    }

    public void addWorker(String username, String name) {
        boolean doesNotExist = workers.stream()
                .noneMatch(worker -> worker.getUsername().equals(username));
        if (doesNotExist) {
            workers.add(new Worker(name, username));
        }
    }

    public void addActivity(String name, int deadline) {
        boolean doesNotExist = selectedProject.getActivities().stream()
                .noneMatch(activity -> activity.getName().equals(name));

        if (doesNotExist) {
            selectedProject.addActivity(new Activity(name, deadline));
        }
        updateSelectedProjectInProjects();
    }

    public void addWorklogToActivity(String name, String description, int estimated) {
        boolean doesNotExist = selectedActivity.getWorklogs().stream()
                .noneMatch(worklog -> worklog.getName().equals(name));
        if (doesNotExist) {
            selectedActivity.addWorklog(new Worklog(name, description, user, estimated));
        }
    }

    public void editSelectedWorklog(String name, String description, int estimated) {
        selectedWorklog.setWorker(user);
        selectedWorklog.setName(name);
        selectedWorklog.setDescription(description);
        selectedWorklog.setTotalEstimated(estimated);
        updateWorklogInActivity();
    }

    public void incrementSelectedWorklogEffort() {
        selectedWorklog.addTotalWorked(1);
        updateWorklogInActivity();
    }

    public void decrementSelectedWorklogEffort() {
        selectedWorklog.decreaseTotalWorked(1);
        updateWorklogInActivity();
    }

    public String getProjectName() {
        return selectedProject.getName();
    }

    public List<Worker> getWorkers() {
        return workers;
    }
    public String getWorkerUsername() {
        return selectedWorker.getUsername();
    }

    public String getActivityName() { return selectedActivity.getName(); }

    public String getWorklogName() { return selectedWorklog.getName();}

    public Worklog getSelectedWorklog() {
        return selectedWorklog;
    }

    public void updateActivityInProject() {
        selectedProject.removeActivityByName(selectedActivity.getName());
        selectedProject.addActivity(selectedActivity);
    }

    public void updateWorklogInActivity() {
        selectedActivity.removeWorklogByName(selectedWorklog.getName());
        selectedActivity.addWorklog(selectedWorklog);
        updateActivityInProject();
        updateSelectedProjectInProjects();

    }
    public void toggleActivityCompletion() {
        selectedActivity.toggleIsCompleted();
        updateActivityInProject();
        updateSelectedProjectInProjects();
    }

    public Boolean getActivityCompletion() {
        return selectedActivity.getIsCompleted();
    }
    public void setProjectName(String name) {
        selectedProject.setName(name);
    }

    public boolean selectedWorkerNotUser() {
        return !selectedWorker.getUsername().equals(user.getUsername());
    }

    public void setActivityName(String name) {
        selectedActivity.setName(name);
        updateActivityInProject();
        updateSelectedProjectInProjects();
    }

    public void setWorkerName(String name) {
        selectedWorker.setName(name);
        updateSelectedWorkerInWorkers();
    }

    public void setWorkerUsername(String username) {
        selectedWorker.setUsername(username);
        updateSelectedWorkerInWorkers();
    }
    public void removeProject() {
        if (selectedProject != null) {
            for (Project project : projects) {
                if (project.getName() == selectedProject.getName()) {
                    projects.remove(project);
                    selectedProject = null;
                    break;
                }
            }
        }
    }

    public void removeWorker() {
        if (selectedWorker != null) {
            for (Worker worker : workers) {
                if (worker.getUsername() == selectedWorker.getUsername()) {
                    workers.remove(worker);
                    selectedWorker = null;
                    break;
                }
            }
        }
    }

    public void removeActivityFromProject() {
        if (selectedActivity != null && selectedProject.getActivities().contains(selectedActivity)) {
            selectedProject.removeActivityByName(selectedActivity.getName());
        }
        selectedActivity = null;
        updateSelectedProjectInProjects();
    }

    public void removeWorklogFromActivity() {
        if (selectedWorklog != null && selectedActivity.getWorklogs().contains(selectedWorklog)) {
            selectedActivity.removeWorklogByName(selectedWorklog.getName());
        }
        selectedWorklog = null;
        updateActivityInProject();
        updateSelectedProjectInProjects();
    }

    public Boolean hasSelectedProject() {
        return selectedProject != null;
    }

    public Boolean hasSelectedActivity() {
        return selectedActivity != null;
    }

    public Boolean hasSelectedWorker() {
        return selectedWorker != null;
    }

    public Boolean hasSelectedWorklog() {
        return selectedWorklog != null;
    }


    public void selectProject(String name) {
        for (Project project : projects) {
            if (project.getName() == name) {
                selectedProject = project;
                selectedActivity = null;
                return;
            }
        }

        selectedProject = null;
    }

    public void selectWorker(String username) {
        for (Worker worker : workers) {
            if (worker.getUsername().equals(username)) {
                selectedWorker = worker;
                return;
            }
        }

        selectedWorker = null;
    }

    public double getSelectedActivityProgress() {
        return selectedActivity.getProgress();
    }

    public LocalDate getSelectedActivityDeadline() {
        return selectedActivity.getDeadline();
    }

    public void setSelectedActivityDeadline(LocalDate deadline) {
        selectedActivity.setDeadline(deadline);
        updateActivityInProject();
        updateSelectedProjectInProjects();
    }

    public void selectActivity(String name) {
        if (hasSelectedProject()) {
            for (Activity activity : selectedProject.getActivities()) {
                if (activity.getName() == name) {
                    selectedActivity = activity;
                    return;
                }
            }
        }

        selectedActivity = null;
    }

    public void selectWorklog(String name) {
        if (hasSelectedActivity()) {
            for (Worklog worklog : selectedActivity.getWorklogs()) {
                if (worklog.getName() == name) {
                    selectedWorklog = worklog;
                    return;
                }
            }
        }

        selectedWorklog = null;
    }
    public Map<String, Object> getSelectedProjectStatistics() {
        Map<String, Object> selectedProjectStats = new HashMap<String, Object>();
        if (selectedProject != null) {
            selectedProjectStats.put("name", selectedProject.getName());
            selectedProjectStats.put("workLeft", selectedProject.getTotalWorkLeft());
            selectedProjectStats.put("totalActivities", selectedProject.getActivities(true).size() + selectedProject.getActivities(false).size());
            selectedProjectStats.put("completedActivities", selectedProject.getActivities(true).size());
        }
        return selectedProjectStats;
    }

    public List<String> getAllProjectLeadersInWorkers() {
        List<String> allWorkersByName = getAllWorkersByName();
        return selectedProject.getProjectLeaders().stream()
                .filter(allWorkersByName::contains)
                .distinct()
                .collect(Collectors.toList()).stream()
                .map(Worker::getName)
                .collect(Collectors.toList());
    }

    public void updateSelectedProjectInProjects() {
        for (int i = 0; i < projects.size(); i++) {
            Project project = projects.get(i);
            if (project.getName() == selectedProject.getName()) {
                projects.set(i, selectedProject);
                break;
            }
        }
    }

    public void updateSelectedWorkerInWorkers() {
        for (int i = 0; i < workers.size(); i++) {
            Worker worker = workers.get(i);
            if (worker.getUsername() == selectedWorker.getUsername()) {
                workers.set(i, selectedWorker);
                break;
            }
        }
    }

    public List<Worker> getSelectedProjectLeaders() {
        return selectedProject.getProjectLeaders();
    }

    public void setLeadersInSelected(List<String> workerNames) {
        for (String name : workerNames) {
            Worker worker = null;
            for (Worker w : workers) {
                if (w.getName().equals(name)) {
                    worker = w;
                    break;
                }
            }
            if (worker != null && !selectedProject.getProjectLeaders().contains(worker)) {
                selectedProject.addProjectLeader(worker);
            }

        }
        updateSelectedProjectInProjects();
    }

    public List<String> getAllCompletedActivities() {
        return selectedProject.getActivities(true).stream()
                .map(activity -> activity.getName())
                .collect(Collectors.toList());
    }

    public List<String> getAllOngoingActivities() {
        return selectedProject.getActivities(false).stream()
                .map(activity -> activity.getName())
                .collect(Collectors.toList());
    }
}
