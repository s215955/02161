package application.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/*
    Mads Anton Frost Kolborg Klynderud (s215955)
*/
public class Project {
    private String name;
    private List<Activity> activities;
    private final List<Worker> projectLeaders;

    public Project(String name) {
        this.name = name;
        this.activities = new ArrayList<Activity>();
        this.projectLeaders = new ArrayList<Worker>();
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Activity> getActivities() {
        return activities;
    }

    public void setActivities(List<Activity> activities) {
        this.activities = activities;
    }

    public void addActivity(Activity activity) {
        this.activities.add(activity);
    }

    public void removeActivityByName(String activityName) {
        for (Activity act : this.activities) {
            if (act.getName() == activityName) {
                this.activities.remove(act);
                break;
            }
        }
    }
    public List<Worker> getProjectLeaders() {
        return projectLeaders;
    }

    public void addProjectLeader(Worker worker) {
        this.projectLeaders.add(worker);
    }

    public int getTotalWorkLeft() {
        AtomicInteger totalWorkLeft = new AtomicInteger(0);
        activities.forEach(activity -> totalWorkLeft.addAndGet(activity.getEstimatedHoursLeft()));
        return totalWorkLeft.get();
    }

    public List<Activity> getActivities(Boolean completed) {

        if (!completed) {
            return this.activities.stream()
                    .filter(activity -> !activity.getIsCompleted())
                    .collect(Collectors.toList());
        }

        return this.activities.stream()
                .filter(activity -> activity.getIsCompleted())
                .collect(Collectors.toList());
    }

    public List<String> getAllActivitiesByName() {
        return this.activities.stream()
                .map(activity -> activity.getName())
                .collect(Collectors.toList());
    }

}
