package application.model;

/*
    Mads Anton Frost Kolborg Klynderud (s215955)
*/
public class Worklog {
    private String name;
    private String description;
    private Worker worker;
    private int totalWorked;
    private int totalEstimated;

    public Worklog(String name, String description, Worker worker, int totalEstimated) {
        this.name = name;
        this.description = description;
        this.worker = worker;
        this.totalWorked = 0;
        this.totalEstimated = totalEstimated;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Worker getWorker() {
        return worker;
    }

    public void setWorker(Worker worker) {
        this.worker = worker;
    }

    public int getTotalWorked() {
        return totalWorked;
    }

    public void setTotalWorked(int totalWorked) {
        this.totalWorked = totalWorked;
    }

    public void addTotalWorked(int worked) {
        this.totalWorked += worked;
    }

    public void decreaseTotalWorked(int worked) {
        this.totalWorked -= worked;
    }

    public int getTotalEstimated() {
        return totalEstimated;
    }

    public void setTotalEstimated(int totalEstimated) {
        this.totalEstimated = totalEstimated;
    }

    public int getHoursLeft() {
        return totalEstimated - totalWorked;
    }

    public double getProgress() {
        if (totalEstimated == 0) {
            return 0.0;
        }
        return (double) totalWorked / totalEstimated;
    }
}
