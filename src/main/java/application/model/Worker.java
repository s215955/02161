package application.model;

/*
    Mads Anton Frost Kolborg Klynderud (s215955)
*/
public class Worker {
    private String name;
    private String username;

    public Worker(String name, String username) {
        this.name = name;
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public String getUsername() {
        return username;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}

