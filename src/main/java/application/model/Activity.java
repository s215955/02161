package application.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/*
    Mads Anton Frost Kolborg Klynderud (s215955)
*/
public class Activity {
    private String name;
    private List<Worklog> worklogs;

    private LocalDate deadline;

    private Boolean isCompleted;

    public Activity(String name, int deadlineInDays) {
        this.name = name;
        this.worklogs = new ArrayList<>();
        this.deadline = LocalDate.now().plusDays(deadlineInDays);
        this.isCompleted = false;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) { this.name = name; }

    public void setDeadline(LocalDate deadline) {
        this.deadline = deadline;
    }

    public LocalDate getDeadline() {
        return deadline;
    }

    public List<Worklog> getWorklogs() {
        return Collections.unmodifiableList(worklogs);
    }

    public void addWorklog(Worklog worklog) {
        worklogs.add(worklog);
    }

    public int getEstimatedHoursLeft() {
        int estimatedHoursLeft = 0;
        for (Worklog worklog : worklogs) {
            estimatedHoursLeft += worklog.getTotalEstimated();
        }
        return estimatedHoursLeft;
    }

    private int getTotalTimeSpent() {
        AtomicInteger totalTimeSpent = new AtomicInteger(0);
        worklogs.forEach(worklog -> totalTimeSpent.addAndGet(worklog.getTotalWorked()));
        return totalTimeSpent.get();
    }

    public int getWorklogSize() {
        return this.worklogs.size();
    }

    public void removeWorklogByName(String name) {
        for (Worklog log : this.worklogs) {
            if (log.getName() == name) {
                this.worklogs.remove(log);
                break;
            }
        }
    }
    public double getProgress() {
        if (getEstimatedHoursLeft() == 0 && getTotalTimeSpent() == 0) {
            return 0;
        }
        double difference = (double) getTotalTimeSpent() / getEstimatedHoursLeft();
        return difference;
    }

    public void toggleIsCompleted() {
        isCompleted = !isCompleted;
    }
    public Boolean getIsCompleted() {
        return isCompleted;
    }

}
