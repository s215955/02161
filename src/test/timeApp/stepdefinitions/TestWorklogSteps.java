package timeApp.stepdefinitions;

import application.model.*;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static org.junit.Assert.assertEquals;

/*
    Mads Anton Frost Kolborg Klynderud (s215955)
*/
public class TestWorklogSteps {
    private Worklog worklog;
    private Worker worker;
    private int estimatedTime;
    private int hoursWorked;
    private int hoursLeft;
    private double progress;

    @Given("I am a worker")
    public void iAmAWorker() {
        worker = new Worker("test", "testuser");
    }

    @When("I create a worklog named {string} with a description {string} and an estimated time of {int} hours")
    public void iCreateAWorklogNamedWithADescriptionAndAnEstimatedTimeOfHours(String name, String description, int estimatedTime) {
        worklog = new Worklog(name, description, worker, estimatedTime);
        this.estimatedTime = estimatedTime;
    }

    @Then("the worklog should have a total estimated time of {int} hours")
    public void theWorklogShouldHaveATotalEstimatedTimeOfHours(int expectedEstimatedTime) {
        assertEquals(expectedEstimatedTime, worklog.getTotalEstimated());
    }

    @And("the worklog should have no hours worked")
    public void theWorklogShouldHaveNoHoursWorked() {
        assertEquals(0, worklog.getTotalWorked());
    }

    @And("the worklog should have a progress of {double}")
    public void theWorklogShouldHaveAProgressOf(double expectedProgress) {
        assertEquals(expectedProgress, worklog.getProgress(), 0.01);
    }

    @Given("I have a worklog named {string}")
    public void iHaveAWorklogNamed(String name) {
        worker = new Worker("test", "testuser");
        worklog = new Worklog(name, "", worker, 10);
        estimatedTime = 10;
    }

    @And("I have worked {int} hours on the worklog")
    public void iHaveWorkedHoursOnTheWorklog(int hoursWorked) {
        worklog.setTotalWorked(hoursWorked);
    }

    @When("I add the hours worked to the worklog")
    public void iAddTheHoursWorkedToTheWorklog() {
        worklog.addTotalWorked(hoursWorked);
    }

    @Then("the worklog should have {int} hours worked")
    public void theWorklogShouldHaveHoursWorked(int expectedHoursWorked) {
        assertEquals(expectedHoursWorked, worklog.getTotalWorked());
    }

    @And("the worklog should have {int} hours left to complete")
    public void theWorklogShouldHaveHoursLeftToComplete(int expectedHoursLeft) {
        assertEquals(expectedHoursLeft, worklog.getTotalEstimated()-worklog.getHoursLeft());
    }

    @When("I decrease the hours worked on the worklog by {int} hours")
    public void iDecreaseTheHoursWorkedOnTheWorklogByHours(int hoursToDecrease) {
        worklog.decreaseTotalWorked(hoursToDecrease);
    }

    @When("I get the hours worked on the worklog")
    public void iGetTheHoursWorkedOnTheWorklog() {
        hoursWorked = worklog.getTotalWorked();
    }

    @Then("I should get {int} hours worked")
    public void iShouldGetHoursWorked(int expectedHoursWorked) {
        assertEquals(expectedHoursWorked, hoursWorked);
    }

    @When("I get the hours left on the worklog")
    public void iGetTheHoursLeftOnTheWorklog() {
        int hoursLeft = worklog.getHoursLeft();
        this.hoursLeft = hoursLeft;
    }

    @Then("I should get {int} hours left to complete")
    public void iShouldGetHoursLeftToComplete(int expectedHoursLeft) {
        assertEquals(expectedHoursLeft, this.hoursLeft);
    }

    @When("I get the progress on the worklog")
    public void iGetTheProgressOnTheWorklog() {
        double progress = worklog.getProgress();
        this.progress = progress;
    }

    @Then("I should get a progress of {double}")
    public void iShouldGetAProgressOf(double expectedProgress) {
        assertEquals(expectedProgress, this.progress, 0.01);
    }
}
