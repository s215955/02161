package timeApp.stepdefinitions;

import application.model.*;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/*
    Mads Anton Frost Kolborg Klynderud (s215955)
*/
public class TestActivitySteps {
    private List<Activity> completed = new ArrayList<Activity>();
    private List<Activity> activities = new ArrayList<Activity>();
    private Activity activity;
    @Given("I have created a new activity named {string} and added it to a list of activities")
    public void iHaveCreatedANewActivityNamed(String arg0) {
        this.activity = new Activity(arg0, 0);
    }

    @And("the activity has a deadline of {int} days from now")
    public void theActivityHasADeadlineOfDaysFromNow(int arg0) {
        this.activity.setDeadline(LocalDate.now().plusDays(arg0));
        this.activities.add(this.activity);
    }

    @Then("I should see {string} in the list")
    public void iShouldSeeInTheList(String arg0) {
        Assert.assertTrue(this.activities.stream().anyMatch(o -> o.getName().equals(arg0)));
    }

    @Given("I have created an activity named {string}")
    public void iHaveCreatedAnActivityNamed2(String arg0) {
        this.activity = new Activity(arg0, 0);
    }

    @When("I add a worklog named {string} with an estimated time of {int} hours to the activity {string}")
    public void iAddAWorklogNamedWithAnEstimatedTimeOfHoursToTheActivity(String arg0, int arg1, String arg2) {
        Worker test = new Worker("mads", "mafr");
        Worklog log = new Worklog(arg0, "desc", test, arg1);
        this.activity.addWorklog(log);
    }

    @Then("the activity {string} should have {int} worklog")
    public void theActivityShouldHaveWorklog(String arg0, int arg1) {
        Assert.assertEquals(this.activity.getWorklogSize(), arg1);
        this.activity.removeWorklogByName(arg0);
    }

    @And("the total estimated time left for the activity should be {int} hours")
    public void theTotalEstimatedTimeLeftForTheActivityShouldBeHours(int arg0) {
        Assert.assertTrue(this.activity.getEstimatedHoursLeft() == arg0);
    }

    @And("I have added a worklog named {string} with an estimated time of {int} hours to the activity {string}")
    public void iHaveAddedAWorklogNamedWithAnEstimatedTimeOfHoursToTheActivity(String arg0, int arg1, String arg2) {
        Worker test = new Worker("mads", "mafr");
        Worklog log = new Worklog(arg0, "desc", test, arg1);
        this.activity.addWorklog(log);
    }

    @When("I remove the worklog named {string} from the activity")
    public void iRemoveTheWorklogNamedFromTheActivity(String arg0) {
        this.activity.removeWorklogByName(arg0);
    }

    @Then("the activity {string} should have {int} worklogs")
    public void theActivityShouldHaveWorklogs(String arg0, int arg1) {
        Assert.assertTrue(this.activity.getWorklogSize() == arg1 + 1);
    }

    @And("the activity is not completed")
    public void theActivityIsNotCompleted() {
        this.activity.getIsCompleted();
    }

    @When("I toggle the completion status of the activity")
    public void iToggleTheCompletionStatusOfTheActivity() {
        this.activity.toggleIsCompleted();
    }

    @Then("the activity should be marked as completed")
    public void theActivityShouldBeMarkedAsCompleted() {
        Assert.assertTrue(this.activity.getIsCompleted());
    }

    @And("the activity is completed")
    public void theActivityIsCompleted() {
    }

    @When("I view the list of completed activities")
    public void iViewTheListOfCompletedActivities() {
        this.completed = this.activities.stream().filter(activity -> activity.getIsCompleted()).collect(Collectors.toList());
    }

    @And("I should not see {string} in the list")
    public void iShouldNotSeeInTheList(String arg0) {
        List<String> activityNames = this.completed.stream().map(Activity::getName).collect(Collectors.toList());
        Assert.assertFalse(activityNames.contains(arg0));
    }
}
