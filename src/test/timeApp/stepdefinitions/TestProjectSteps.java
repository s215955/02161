package timeApp.stepdefinitions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import application.model.*;

/*
    Mads Anton Frost Kolborg Klynderud (s215955)
*/
public class TestProjectSteps {
    private Project project;
    private List<Worker> projectLeaders;
    private List<Activity> activities;
    private Activity activity;
    private List<Activity> completedActivities;

    @When("I create a project named {string}")
    public void iCreateAProjectNamed(String arg0) {
        project = new Project(arg0);
    }

    @Then("the project should have no activities")
    public void theProjectShouldHaveNoActivities() {
        assertTrue(project.getActivities().isEmpty());
    }

    @And("the project should have no project leaders")
    public void theProjectShouldHaveNoProjectLeaders() {
        assertTrue(project.getProjectLeaders().isEmpty());
    }

    @Given("I have a project named {string}")
    public void iHaveAProjectNamed(String projectName) {
        project = new Project(projectName);
    }

    @And("I have a list of project leaders")
    public void iHaveAListOfProjectLeaders() {
        projectLeaders = new ArrayList<>();
        projectLeaders.add(new Worker("JavaScript IsBad", "jsib"));
        projectLeaders.add(new Worker("TypeScript IsTheBest", "tsistb"));
    }

    @When("I add the project leaders to the project")
    public void iAddTheProjectLeadersToTheProject() {
        for (Worker leader : projectLeaders) {
            project.addProjectLeader(leader);
        }
    }

    @Then("the project should have the project leaders")
    public void theProjectShouldHaveTheProjectLeaders() {
        assertEquals(projectLeaders, project.getProjectLeaders());
    }

    @And("I have a list of activities")
    public void iHaveAListOfActivities() {
        activities = new ArrayList<>();
        activities.add(new Activity("Design", 3));
        activities.add(new Activity("Implementation", 4));
        activities.add(new Activity("Testing", 5));
    }

    @When("I add the activities to the project")
    public void iAddTheActivitiesToTheProject() {
        for (Activity activity : activities) {
            project.addActivity(activity);
        }
    }

    @Then("the project should have the activities")
    public void theProjectShouldHaveTheActivities() {
        assertEquals(activities, project.getActivities());
    }

    @And("the project has an activity named {string}")
    public void theProjectHasAnActivityNamed(String activityName) {
        activity = new Activity(activityName, 5);
        project.addActivity(activity);
    }

    @Then("the project should now have the activity named {string}")
    public void theProjectShouldNotHaveTheActivityNamed(String arg0) {
        assertTrue(project.getAllActivitiesByName().contains(arg0));
    }

    @Given("the project has some completed activities")
    public void theProjectHasSomeCompletedActivities() {
        activities = new ArrayList<>();
        Activity activity1 = new Activity("Activity 1", 10);
        activity1.toggleIsCompleted();
        activities.add(activity1);
        activities.add(new Activity("Activity 2", 5));
        project.setActivities(activities);
        assertTrue(project.getActivities().stream().filter(activity -> activity.getIsCompleted()).collect(Collectors.toList()).size() >= 1);
    }

    @Given("the project has some incomplete activities")
    public void theProjectHasSomeIncompleteActivities() {
        activities = new ArrayList<>();
        activities.add(new Activity("Activity 1", 10));
        activities.add(new Activity("Activity 2", 5));
        project.setActivities(activities);
        assertTrue(project.getActivities().stream().filter(activity1 -> !activity1.getIsCompleted()).collect(Collectors.toList()).size() >= 1);
    }


    @When("I remove the activity named {string} from the project")
    public void iRemoveTheActivityNamedFromTheProject(String activityName) {
        project.removeActivityByName(activityName);
    }

    @When("I calculate the total work left in the project")
    public void iCalculateTheTotalWorkLeftInTheProject() {
        project.getTotalWorkLeft();
    }

    @When("I get the list of completed activities in the project")
    public void iGetTheListOfCompletedActivitiesInTheProject() {
        this.completedActivities = project.getActivities().stream().filter(activity -> activity.getIsCompleted()).collect(Collectors.toList());
    }

    @Then("I should get the correct list of completed activities")
    public void iShouldGetTheCorrectListOfCompletedActivities() {
        assertTrue(this.completedActivities != null);
    }
}