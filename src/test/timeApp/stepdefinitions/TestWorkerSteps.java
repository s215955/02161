package timeApp.stepdefinitions;

import application.model.Worker;
import application.model.Worklog;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

/*
    Mads Anton Frost Kolborg Klynderud (s215955)
*/
public class TestWorkerSteps {
    private Worker currentWorker;
    private Worklog currentWorklog;

    private String username;
    private String name;
    @Given("I have a name {string}")
    public void iHaveAName(String arg0) {
        this.name = arg0;
    }

    @And("I have a username {string}")
    public void iHaveAUsername(String arg0) {
        this.username = arg0;
    }

    @When("I create a new worker with name and username")
    public void iCreateANewWorkerWithNameAndUsername() {
        currentWorker = new Worker(name, username);
    }

    @Then("the worker's name should be {string}")
    public void theWorkerSNameShouldBe(String arg0) {
        Assert.assertEquals(currentWorker.getName(), arg0);
    }

    @And("the worker's username should be {string}")
    public void theWorkerSUsernameShouldBe(String arg0) {
        Assert.assertEquals(currentWorker.getUsername(), arg0);
    }
}
