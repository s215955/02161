Feature: Manage Worklogs

  Scenario: Create a new worklog
    Given I am a worker
    When I create a worklog named "New Worklog" with a description "Worklog description" and an estimated time of 10 hours
    Then the worklog should have a total estimated time of 10 hours
    And the worklog should have no hours worked
    And the worklog should have a progress of 0.0

  Scenario: Add hours worked to a worklog
    Given I have a worklog named "New Worklog"
    And I have worked 5 hours on the worklog
    When I add the hours worked to the worklog
    Then the worklog should have 5 hours worked
    And the worklog should have 5 hours left to complete
    And the worklog should have a progress of 0.5

  Scenario: Decrease hours worked on a worklog
    Given I have a worklog named "New Worklog"
    And I have worked 5 hours on the worklog
    When I decrease the hours worked on the worklog by 3 hours
    Then the worklog should have 2 hours worked
    And the worklog should have 2 hours left to complete
    And the worklog should have a progress of 0.2

  Scenario: Get hours worked on a worklog
    Given I have a worklog named "New Worklog"
    And I have worked 5 hours on the worklog
    When I get the hours worked on the worklog
    Then I should get 5 hours worked

  Scenario: Get hours left on a worklog
    Given I have a worklog named "New Worklog"
    And I have worked 5 hours on the worklog
    When I get the hours left on the worklog
    Then I should get 5 hours left to complete

  Scenario: Get progress on a worklog
    Given I have a worklog named "New Worklog"
    And I have worked 5 hours on the worklog
    When I get the progress on the worklog
    Then I should get a progress of 0.5
