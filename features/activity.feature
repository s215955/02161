Feature: Manage activities

  Scenario: Create a new activity
    Given I have created a new activity named "Project planning" and added it to a list of activities
    And the activity has a deadline of 10 days from now
    Then I should see "Project planning" in the list

  Scenario: Add worklog to an activity
    Given I have created an activity named "Design UI"
    When I add a worklog named "Initial design" with an estimated time of 5 hours to the activity "Design UI"
    Then the activity "Design UI" should have 1 worklog
    And the total estimated time left for the activity should be 5 hours

  Scenario: Remove worklog from an activity
    Given I have created an activity named "Design UI"
    And I have added a worklog named "Initial design" with an estimated time of 5 hours to the activity "Design UI"
    When I remove the worklog named "Initial design" from the activity
    Then the activity "Design UI" should have 0 worklogs
    And the total estimated time left for the activity should be 10 hours

  Scenario: Toggle completion status of an activity
    Given I have created an activity named "Design UI"
    And the activity is not completed
    When I toggle the completion status of the activity
    Then the activity should be marked as completed

  Scenario: View activities by completion status
    Given I have created an activity named "Design UI"
    And the activity is completed
    And I have created an activity named "Backend development"
    And the activity is not completed
    When I view the list of completed activities
    Then I should see "Design UI" in the list
    And I should not see "Backend development" in the list
