Feature: Manage Projects

  Scenario: Create a new project
    When I create a project named "New Project"
    Then the project should have no activities
    And the project should have no project leaders

  Scenario: Add project leaders to a project
    Given I have a project named "New Project"
    And I have a list of project leaders
    When I add the project leaders to the project
    Then the project should have the project leaders

  Scenario: Add activities to a project
    Given I have a project named "New Project"
    And I have a list of activities
    When I add the activities to the project
    Then the project should have the activities

  Scenario: Remove an activity from a project
    Given I have a project named "New Project"
    And the project has an activity named "Activity 1"
    When I remove the activity named "Activity 1" from the project
    Then the project should now have the activity named "Activity 1"


  Scenario: Get the list of completed activities in a project
    Given I have a project named "New Project"
    And the project has some completed activities
    When I get the list of completed activities in the project
    Then I should get the correct list of completed activities
