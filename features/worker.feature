Feature: Worker

  Scenario: Creating a new worker
    Given I have a name "Gruppe18"
    And I have a username "g18"
    When I create a new worker with name and username
    Then the worker's name should be "Gruppe18"
    And the worker's username should be "g18"